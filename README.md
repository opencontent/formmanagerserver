# Form Manager

Implementation of customized form.io APIs

## Description

Implementation of form.io APIs limited to Forms and Submissions. Those APIs offer CRUD operations
over form.io Forms and Submissions.

### Form Definition

| Field                   	| Definition                                                |
| ------------------------	| --------------------------------------------------------- |
| `title`    	      	    | Title of the Service                                      |
| `name`			        | Name of the Service                                       |
| `path`		            | Path related to the Service                               |
| `description`		        | Description of the Service                                |
| `tags`		            | Tag of the service. Can be `component`, `basic`, `custom` |
| `components`		        | JSON definition of form components                        |
| `type`    	      	    | Type of the form. Can be `form` or `resource`             |
| `display`			        | Type of form visualization. Can be `form` or `wizard`     |
| `created`		            | Creation Date of the form                                 |
| `modified`		        | Last modification Date of the form                        |

### Subscription Definition

| Field                   	| Definition                                                |
| ------------------------	| --------------------------------------------------------- |
| `data`    	      	    | Data of the Submission                                    |
| `form`			        | Id of the submitted form                                  |
| `status`		            | Status of the submission (To define)                      |
| `created`		            | Creation Date of the submission                           |
| `modified`		        | Last modification Date of the submission                  |

## Install

    docker build -t formmanagerserver .
    docker run --name formserver --rm formmanagerserver
    
## Services

## Form Services

Here's a list of all services concerning Forms:

 * Retrieve all Forms [`GET /form`]
 * Create a Form [`POST /form`]
 * Retrieve a Form by its identifier [`GET /form/{formId}`]
 * Retrieve a Form Schema by form identifier [`GET /form/{formId}/schema`]
 * Retrieve a Form [`GET /{path}`]
 * Retrieve a Form Schema [`GET /{path}/schema`]
 * Edit an existing Form [`PUT /{path}`]
 * Delete an existing Form [`DELETE /{path}`]
 * Delete an existing Form by form identifier [`DELETE /form/{formId}`]
 * Convert an existing wizard to a form display [`GET /printable/{formId}`]
 
## Submission Services

Here's a list of all services concerning Submissions:

 * Retrieve all Submissions of a given Form [`GET /{path}/submission`]
 * Create a Submission [`POST /{path}/submission`]
 * Retrieve a Submission by its identifier [`GET /{path}/submission/{submissionId}`]
 * Edit an existing Submission [`PUT /{path}/submission/{submissionId}`]
 * Delete an existing Submission [`DELETE /{path}/submission/{submissionId}`]
 
# REST API

## Documentation

API documentation is available at the `/api-docs` endpoint

## Retrieve all Forms [`GET /form`]

Retrieve all Forms: Default Results will include forms with `basic` or `custom` tag

### Parameters

* name - Name of the Form
* select - Select fields to return
* exclude_tags - Category of tags that will be excluded from the result set

Request :

`GET /form`

Response:

    200 OK (application/json)
    
    [
        {
            "display": "form",
            "type": "form",
            "components": [
                {
                    "label": "Columns",
                    "input": false,
                    "tableView": false,
                    "key": "columns",
                    "columns": [
                        {
                            "components": [
                                {
                                    "label": "Nome",
                                    "placeholder": "Mario",
                                    "allowMultipleMasks": false,
                                    "showWordCount": false,
                                    "showCharCount": false,
                                    "clearOnHide": false,
                                    "tableView": true,
                                    "alwaysEnabled": false,
                                    "type": "textfield",
                                    "input": true,
                                    "key": "nome",
                                    "defaultValue": "",
                                    "spellcheck": false,
                                    "validate": {
                                        "pattern": "^([a-zA-Z ])+$",
                                        "customMessage": "Il nome può contenere solo caratteri alfabetici",
                                        "json": ""
                                    },
                                    "conditional": {
                                        "show": "",
                                        "when": "",
                                        "json": ""
                                    },
                                    "inputFormat": "plain",
                                    "tags": [],
                                    "encrypted": false,
                                    "customConditional": "",
                                    "logic": [],
                                    "widget": {
                                        "type": ""
                                    },
                                    "reorder": false
                                }
                            ],
                            "width": 6,
                            "offset": 0,
                            "push": 0,
                            "pull": 0,
                            "type": "column",
                            "input": false,
                            "hideOnChildrenHidden": false,
                            "key": "column",
                            "tableView": true,
                            "label": "Column"
                        },
                        {
                            "components": [
                                {
                                    "label": "Cognome",
                                    "placeholder": "Rossi",
                                    "allowMultipleMasks": false,
                                    "showWordCount": false,
                                    "showCharCount": false,
                                    "clearOnHide": false,
                                    "tableView": true,
                                    "alwaysEnabled": false,
                                    "type": "textfield",
                                    "input": true,
                                    "key": "cognome",
                                    "defaultValue": "",
                                    "spellcheck": false,
                                    "validate": {
                                        "pattern": "^([a-zA-Z ])+$",
                                        "customMessage": "Il cognome può contenere solo caratteri alfabetici",
                                        "json": ""
                                    },
                                    "conditional": {
                                        "show": "",
                                        "when": "",
                                        "json": ""
                                    },
                                    "inputFormat": "plain",
                                    "tags": [],
                                    "widget": {
                                        "type": ""
                                    },
                                    "reorder": false,
                                    "encrypted": false,
                                    "customConditional": "",
                                    "logic": []
                                }
                            ],
                            "width": 6,
                            "offset": 0,
                            "push": 0,
                            "pull": 0,
                            "type": "column",
                            "input": false,
                            "hideOnChildrenHidden": false,
                            "key": "column",
                            "tableView": true,
                            "label": "Column"
                        }
                    ],
                    "type": "columns",
                    "hideLabel": true,
                    "tags": [],
                    "conditional": {
                        "show": ""
                    }
                },
                {
                    "label": "Submit",
                    "state": "",
                    "theme": "primary",
                    "shortcut": "",
                    "mask": false,
                    "tableView": true,
                    "alwaysEnabled": false,
                    "type": "button",
                    "input": true,
                    "key": "submit2",
                    "defaultValue": false,
                    "validate": {
                        "customMessage": "",
                        "json": ""
                    },
                    "conditional": {
                        "show": "",
                        "when": "",
                        "json": ""
                    },
                    "showValidations": false,
                    "event": "",
                    "url": "",
                    "custom": "",
                    "reorder": false,
                    "encrypted": false,
                    "customConditional": "",
                    "logic": []
                }
            ],
            "tags": [
                "component"
            ],
            "_id": "5d4d26ff9410f50010f30068",
            "title": "NeC",
            "name": "neC",
            "path": "nec",
            "description": "Nome e Cognome",
            "machineName": "neC",
            "modified": "2019-09-06T09:14:17.918Z",
            "created": "2019-09-06T09:14:17.918Z",
            "__v": 0
        },
        {
            "display": "form",
            "type": "form",
            "components": [
                {
                    "autofocus": false,
                    "input": true,
                    "tableView": true,
                    "inputType": "number",
                    "label": "Euro",
                    "key": "euro",
                    "placeholder": "0,00",
                    "prefix": "€",
                    "suffix": "",
                    "defaultValue": "",
                    "protected": false,
                    "persistent": true,
                    "hidden": false,
                    "clearOnHide": false,
                    "validate": {
                        "required": false,
                        "min": 0,
                        "max": "",
                        "step": "any",
                        "integer": "",
                        "multiple": "",
                        "custom": ""
                    },
                    "type": "number",
                    "labelPosition": "top",
                    "tags": [],
                    "conditional": {
                        "show": "",
                        "when": null,
                        "eq": ""
                    },
                    "hideLabel": true,
                    "delimiter": true,
                    "decimalLimit": 2,
                    "requireDecimal": false
                }
            ],
            "tags": [
                "component"
            ],
            "_id": "5d4d310a9410f50010f3006a",
            "owner": "5d25902b9df0701900c9c015",
            "title": "Valore monetario",
            "name": "valoreMonetario",
            "path": "euro",
            "description": "Valore Monetario",
            "machineName": "valoreMonetario",
            "modified": "2019-09-05T14:46:55.637Z",
            "created": "2019-09-05T14:46:55.637Z",
            "__v": 0
        }
    ]
        
Request:

`GET /form?name=nec`

Response:

    200 OK (application/json)
    
    [
        {
            "display": "form",
            "type": "form",
            "components": [
                {
                    "label": "Columns",
                    "input": false,
                    "tableView": false,
                    "key": "columns",
                    "columns": [
                        {
                            "components": [
                                {
                                    "label": "Nome",
                                    "placeholder": "Mario",
                                    "allowMultipleMasks": false,
                                    "showWordCount": false,
                                    "showCharCount": false,
                                    "clearOnHide": false,
                                    "tableView": true,
                                    "alwaysEnabled": false,
                                    "type": "textfield",
                                    "input": true,
                                    "key": "nome",
                                    "defaultValue": "",
                                    "spellcheck": false,
                                    "validate": {
                                        "pattern": "^([a-zA-Z ])+$",
                                        "customMessage": "Il nome può contenere solo caratteri alfabetici",
                                        "json": ""
                                    },
                                    "conditional": {
                                        "show": "",
                                        "when": "",
                                        "json": ""
                                    },
                                    "inputFormat": "plain",
                                    "tags": [],
                                    "encrypted": false,
                                    "customConditional": "",
                                    "logic": [],
                                    "widget": {
                                        "type": ""
                                    },
                                    "reorder": false
                                }
                            ],
                            "width": 6,
                            "offset": 0,
                            "push": 0,
                            "pull": 0,
                            "type": "column",
                            "input": false,
                            "hideOnChildrenHidden": false,
                            "key": "column",
                            "tableView": true,
                            "label": "Column"
                        },
                        {
                            "components": [
                                {
                                    "label": "Cognome",
                                    "placeholder": "Rossi",
                                    "allowMultipleMasks": false,
                                    "showWordCount": false,
                                    "showCharCount": false,
                                    "clearOnHide": false,
                                    "tableView": true,
                                    "alwaysEnabled": false,
                                    "type": "textfield",
                                    "input": true,
                                    "key": "cognome",
                                    "defaultValue": "",
                                    "spellcheck": false,
                                    "validate": {
                                        "pattern": "^([a-zA-Z ])+$",
                                        "customMessage": "Il cognome può contenere solo caratteri alfabetici",
                                        "json": ""
                                    },
                                    "conditional": {
                                        "show": "",
                                        "when": "",
                                        "json": ""
                                    },
                                    "inputFormat": "plain",
                                    "tags": [],
                                    "widget": {
                                        "type": ""
                                    },
                                    "reorder": false,
                                    "encrypted": false,
                                    "customConditional": "",
                                    "logic": []
                                }
                            ],
                            "width": 6,
                            "offset": 0,
                            "push": 0,
                            "pull": 0,
                            "type": "column",
                            "input": false,
                            "hideOnChildrenHidden": false,
                            "key": "column",
                            "tableView": true,
                            "label": "Column"
                        }
                    ],
                    "type": "columns",
                    "hideLabel": true,
                    "tags": [],
                    "conditional": {
                        "show": ""
                    }
                },
                {
                    "label": "Submit",
                    "state": "",
                    "theme": "primary",
                    "shortcut": "",
                    "mask": false,
                    "tableView": true,
                    "alwaysEnabled": false,
                    "type": "button",
                    "input": true,
                    "key": "submit2",
                    "defaultValue": false,
                    "validate": {
                        "customMessage": "",
                        "json": ""
                    },
                    "conditional": {
                        "show": "",
                        "when": "",
                        "json": ""
                    },
                    "showValidations": false,
                    "event": "",
                    "url": "",
                    "custom": "",
                    "reorder": false,
                    "encrypted": false,
                    "customConditional": "",
                    "logic": []
                }
            ],
            "tags": [
                "component"
            ],
            "_id": "5d4d26ff9410f50010f30068",
            "title": "NeC",
            "name": "neC",
            "path": "nec",
            "description": "Nome e Cognome",
            "machineName": "neC",
            "modified": "2019-09-06T09:14:17.918Z",
            "created": "2019-09-06T09:14:17.918Z",
            "__v": 0
        }
    ]
    
Request:

`GET /form?exclude_tags=component,basic`

Response:
    
    [
        {
            "display": "form",
            "type": "form",
            "components": [
                {
                    "label": "Text Field",
                    "allowMultipleMasks": false,
                    "showWordCount": false,
                    "showCharCount": false,
                    "tableView": true,
                    "alwaysEnabled": false,
                    "type": "textfield",
                    "input": true,
                    "key": "textField2",
                    "defaultValue": "",
                    "validate": {
                        "customMessage": "",
                        "json": "",
                        "required": false,
                        "custom": "",
                        "customPrivate": false,
                        "minLength": "",
                        "maxLength": "",
                        "minWords": "",
                        "maxWords": "",
                        "pattern": ""
                    },
                    "conditional": {
                        "show": "",
                        "when": "",
                        "json": "",
                        "eq": ""
                    },
                    "widget": {
                        "type": "",
                        "format": "yyyy-MM-dd hh:mm a",
                        "dateFormat": "yyyy-MM-dd hh:mm a",
                        "saveAs": "text"
                    },
                    "reorder": false,
                    "inputFormat": "plain",
                    "encrypted": false,
                    "customConditional": "",
                    "logic": [],
                    "placeholder": "",
                    "prefix": "",
                    "customClass": "",
                    "suffix": "",
                    "multiple": false,
                    "protected": false,
                    "unique": false,
                    "persistent": true,
                    "hidden": false,
                    "clearOnHide": true,
                    "dataGridLabel": false,
                    "labelPosition": "top",
                    "labelWidth": 30,
                    "labelMargin": 3,
                    "description": "",
                    "errorLabel": "",
                    "tooltip": "",
                    "hideLabel": false,
                    "tabindex": "",
                    "disabled": false,
                    "autofocus": false,
                    "dbIndex": false,
                    "customDefaultValue": "",
                    "calculateValue": "",
                    "allowCalculateOverride": false,
                    "refreshOn": "",
                    "clearOnRefresh": false,
                    "validateOn": "change",
                    "mask": false,
                    "inputType": "text",
                    "inputMask": "",
                    "id": "emxf8l"
                },
                {
                    "label": "Text Field",
                    "allowMultipleMasks": false,
                    "showWordCount": false,
                    "showCharCount": false,
                    "tableView": true,
                    "alwaysEnabled": false,
                    "type": "textfield",
                    "input": true,
                    "key": "textField3",
                    "defaultValue": "",
                    "validate": {
                        "customMessage": "",
                        "json": "",
                        "required": false,
                        "custom": "",
                        "customPrivate": false,
                        "minLength": "",
                        "maxLength": "",
                        "minWords": "",
                        "maxWords": "",
                        "pattern": ""
                    },
                    "conditional": {
                        "show": "",
                        "when": "",
                        "json": "",
                        "eq": ""
                    },
                    "widget": {
                        "type": "",
                        "format": "yyyy-MM-dd hh:mm a",
                        "dateFormat": "yyyy-MM-dd hh:mm a",
                        "saveAs": "text"
                    },
                    "reorder": false,
                    "inputFormat": "plain",
                    "encrypted": false,
                    "customConditional": "",
                    "logic": [],
                    "placeholder": "",
                    "prefix": "",
                    "customClass": "",
                    "suffix": "",
                    "multiple": false,
                    "protected": false,
                    "unique": false,
                    "persistent": true,
                    "hidden": false,
                    "clearOnHide": true,
                    "dataGridLabel": false,
                    "labelPosition": "top",
                    "labelWidth": 30,
                    "labelMargin": 3,
                    "description": "",
                    "errorLabel": "",
                    "tooltip": "",
                    "hideLabel": false,
                    "tabindex": "",
                    "disabled": false,
                    "autofocus": false,
                    "dbIndex": false,
                    "customDefaultValue": "",
                    "calculateValue": "",
                    "allowCalculateOverride": false,
                    "refreshOn": "",
                    "clearOnRefresh": false,
                    "validateOn": "change",
                    "mask": false,
                    "inputType": "text",
                    "inputMask": "",
                    "id": "ewmxm74e"
                },
                {
                    "type": "button",
                    "label": "Submit",
                    "key": "submit",
                    "disableOnInvalid": true,
                    "theme": "primary",
                    "input": true,
                    "tableView": true,
                    "placeholder": "",
                    "prefix": "",
                    "customClass": "",
                    "suffix": "",
                    "multiple": false,
                    "defaultValue": null,
                    "protected": false,
                    "unique": false,
                    "persistent": false,
                    "hidden": false,
                    "clearOnHide": true,
                    "dataGridLabel": true,
                    "labelPosition": "top",
                    "labelWidth": 30,
                    "labelMargin": 3,
                    "description": "",
                    "errorLabel": "",
                    "tooltip": "",
                    "hideLabel": false,
                    "tabindex": "",
                    "disabled": false,
                    "autofocus": false,
                    "dbIndex": false,
                    "customDefaultValue": "",
                    "calculateValue": "",
                    "allowCalculateOverride": false,
                    "widget": null,
                    "refreshOn": "",
                    "clearOnRefresh": false,
                    "validateOn": "change",
                    "validate": {
                        "required": false,
                        "custom": "",
                        "customPrivate": false
                    },
                    "conditional": {
                        "show": null,
                        "when": null,
                        "eq": ""
                    },
                    "size": "md",
                    "leftIcon": "",
                    "rightIcon": "",
                    "block": false,
                    "action": "submit",
                    "id": "ex3gatm"
                }
            ],
            "tags": [
                "custom"
            ],
            "_id": "5d77794fb76d04001f2813d7",
            "title": "Prova",
            "name": "Prova",
            "path": "prova",
            "description": "Prova descrizione servizio",
            "modified": "2019-09-10T10:22:07.459Z",
            "created": "2019-09-10T10:22:07.459Z",
            "__v": 0
        }
    ]

## Retrieve Form by Id [`GET /form/{id}`]

### Parameters
* id - Identifier of the form to retrieve

Request:

`GET /form/5d77794fb76d04001f2813d7`

Response:

    200 OK (application/json)
    
    {
        "display": "form",
        "type": "form",
        "components": [
            {
                "label": "Text Field",
                "allowMultipleMasks": false,
                "showWordCount": false,
                "showCharCount": false,
                "tableView": true,
                "alwaysEnabled": false,
                "type": "textfield",
                "input": true,
                "key": "textField2",
                "defaultValue": "",
                "validate": {
                    "customMessage": "",
                    "json": "",
                    "required": false,
                    "custom": "",
                    "customPrivate": false,
                    "minLength": "",
                    "maxLength": "",
                    "minWords": "",
                    "maxWords": "",
                    "pattern": ""
                },
                "conditional": {
                    "show": "",
                    "when": "",
                    "json": "",
                    "eq": ""
                },
                "widget": {
                    "type": "",
                    "format": "yyyy-MM-dd hh:mm a",
                    "dateFormat": "yyyy-MM-dd hh:mm a",
                    "saveAs": "text"
                },
                "reorder": false,
                "inputFormat": "plain",
                "encrypted": false,
                "customConditional": "",
                "logic": [],
                "placeholder": "",
                "prefix": "",
                "customClass": "",
                "suffix": "",
                "multiple": false,
                "protected": false,
                "unique": false,
                "persistent": true,
                "hidden": false,
                "clearOnHide": true,
                "dataGridLabel": false,
                "labelPosition": "top",
                "labelWidth": 30,
                "labelMargin": 3,
                "description": "",
                "errorLabel": "",
                "tooltip": "",
                "hideLabel": false,
                "tabindex": "",
                "disabled": false,
                "autofocus": false,
                "dbIndex": false,
                "customDefaultValue": "",
                "calculateValue": "",
                "allowCalculateOverride": false,
                "refreshOn": "",
                "clearOnRefresh": false,
                "validateOn": "change",
                "mask": false,
                "inputType": "text",
                "inputMask": "",
                "id": "emxf8l"
            },
            {
                "label": "Text Field",
                "allowMultipleMasks": false,
                "showWordCount": false,
                "showCharCount": false,
                "tableView": true,
                "alwaysEnabled": false,
                "type": "textfield",
                "input": true,
                "key": "textField3",
                "defaultValue": "",
                "validate": {
                    "customMessage": "",
                    "json": "",
                    "required": false,
                    "custom": "",
                    "customPrivate": false,
                    "minLength": "",
                    "maxLength": "",
                    "minWords": "",
                    "maxWords": "",
                    "pattern": ""
                },
                "conditional": {
                    "show": "",
                    "when": "",
                    "json": "",
                    "eq": ""
                },
                "widget": {
                    "type": "",
                    "format": "yyyy-MM-dd hh:mm a",
                    "dateFormat": "yyyy-MM-dd hh:mm a",
                    "saveAs": "text"
                },
                "reorder": false,
                "inputFormat": "plain",
                "encrypted": false,
                "customConditional": "",
                "logic": [],
                "placeholder": "",
                "prefix": "",
                "customClass": "",
                "suffix": "",
                "multiple": false,
                "protected": false,
                "unique": false,
                "persistent": true,
                "hidden": false,
                "clearOnHide": true,
                "dataGridLabel": false,
                "labelPosition": "top",
                "labelWidth": 30,
                "labelMargin": 3,
                "description": "",
                "errorLabel": "",
                "tooltip": "",
                "hideLabel": false,
                "tabindex": "",
                "disabled": false,
                "autofocus": false,
                "dbIndex": false,
                "customDefaultValue": "",
                "calculateValue": "",
                "allowCalculateOverride": false,
                "refreshOn": "",
                "clearOnRefresh": false,
                "validateOn": "change",
                "mask": false,
                "inputType": "text",
                "inputMask": "",
                "id": "ewmxm74e"
            },
            {
                "type": "button",
                "label": "Submit",
                "key": "submit",
                "disableOnInvalid": true,
                "theme": "primary",
                "input": true,
                "tableView": true,
                "placeholder": "",
                "prefix": "",
                "customClass": "",
                "suffix": "",
                "multiple": false,
                "defaultValue": null,
                "protected": false,
                "unique": false,
                "persistent": false,
                "hidden": false,
                "clearOnHide": true,
                "dataGridLabel": true,
                "labelPosition": "top",
                "labelWidth": 30,
                "labelMargin": 3,
                "description": "",
                "errorLabel": "",
                "tooltip": "",
                "hideLabel": false,
                "tabindex": "",
                "disabled": false,
                "autofocus": false,
                "dbIndex": false,
                "customDefaultValue": "",
                "calculateValue": "",
                "allowCalculateOverride": false,
                "widget": null,
                "refreshOn": "",
                "clearOnRefresh": false,
                "validateOn": "change",
                "validate": {
                    "required": false,
                    "custom": "",
                    "customPrivate": false
                },
                "conditional": {
                    "show": null,
                    "when": null,
                    "eq": ""
                },
                "size": "md",
                "leftIcon": "",
                "rightIcon": "",
                "block": false,
                "action": "submit",
                "id": "ex3gatm"
            }
        ],
        "tags": [
            "custom"
        ],
        "_id": "5d77794fb76d04001f2813d7",
        "title": "Prova",
        "name": "Prova",
        "path": "prova",
        "description": "Prova descrizione servizio",
        "modified": "2019-09-10T10:22:07.459Z",
        "created": "2019-09-10T10:22:07.459Z",
        "__v": 0
    }
    
Request:

`GET /form/123`

Response:
    
    404 Not Found (application/json)
    
    {
        "title": "Not Found",
        "detail": "Invalid Id",
        "status": 404,
        "instance": {
            "message": "Cast to ObjectId failed for value \"123\" at path \"_id\" for model \"Form\"",
            "name": "CastError",
            "stringValue": "\"123\"",
            "kind": "ObjectId",
            "value": "123",
            "path": "_id"
        }
    }
    
Request:

`GET /form/5d4d26ff9410f50010f30068s`

Response:
    
    400 Bad Request (application/json)
    
    {
        "title": "Bad Request",
        "detail": "Invalid Parameters",
        "status": 400,
        "instance": {
            "message": "Cast to ObjectId failed for value \"5d4d26ff9410f50010f30068s\" at path \"_id\" for model \"Form\"",
            "name": "CastError",
            "stringValue": "\"5d4d26ff9410f50010f30068s\"",
            "kind": "ObjectId",
            "value": "5d4d26ff9410f50010f30068s",
            "path": "_id"
        }
    }
    
## Retrieve Form Schema by Form Id [`GET /form/{id}/schema`]

### Parameters
* id - Identifier of the form to retrieve

Request:

`GET /form/5d4d26ff9410f50010f30068/schema`

Response:

    200 OK (application/json)
    
    {
      "name": {
        "label": "Nome",
        "type": "textfield"
      },
      "surname": {
        "label": "Cognome",
        "type": "textfield"
      }
    }
    
Request:

`GET /form/123/schema`

Response:
    
    404 Not Found (application/json)
    
    {
      "title": "Not Found",
      "detail": "Form not found",
      "status": 404,
      "instance": "/form/5d4a26ff9410f50010f30068/schema"
    }
    
Request:

`GET /form/123/schema`

Response:
    
    400 Bad Request (application/json)
    
    {
      "title": "Bad Request",
      "detail": "Invalid Id",
      "status": 400,
      "instance": {
        "message": "Cast to ObjectId failed for value \"123\" at path \"_id\" for model \"Form\"",
        "name": "CastError",
        "stringValue": "\"123\"",
        "kind": "ObjectId",
        "value": "123",
        "path": "_id"
      }
    }
    
## Convert a Form by Form Id [`GET /printable/{id}`]

### Parameters
* id - Identifier of the form to convert

Request:

`GET /printable/5d7a34027672e80020c95c01`

Response:

    200 OK (application/json)
    
    {
      "display": "form",
      "type": "form",
      "components": [
        {
          "label": "Panel",
          "title": "Pagina 1",
          "breadcrumbClickable": true,
          "buttonSettings": {
            "previous": true,
            "cancel": true,
            "next": true
          },
          "collapsible": false,
          "mask": false,
          "tableView": false,
          "alwaysEnabled": false,
          "type": "panel",
          "input": false,
          "components": [
            {
              "label": "Sono il responsabile di un'azienda o società",
              "shortcut": "",
              "mask": false,
              "tableView": true,
              "alwaysEnabled": false,
              "type": "checkbox",
              "input": true,
              "key": "sonoIlResponsabileDiUnaziendaOSocieta",
              "defaultValue": false,
              "validate": {
                "customMessage": "",
                "json": ""
              },
              "conditional": {
                "show": "",
                "when": "",
                "json": ""
              },
              "reorder": false,
              "encrypted": false,
              "customConditional": "",
              "logic": [
                
              ]
            },
            {
              "label": "Content",
              "className": "",
              "refreshOnChange": false,
              "mask": false,
              "tableView": true,
              "alwaysEnabled": false,
              "type": "content",
              "input": false,
              "key": "content2",
              "validate": {
                "customMessage": "",
                "json": ""
              },
              "conditional": {
                "show": "",
                "when": "",
                "json": ""
              },
              "reorder": false,
              "encrypted": false,
              "customConditional": "",
              "logic": [
                
              ],
              "html": "<p><span style=\"color:rgb(0,0,0);\">Il/la sottoscritto/a</span></p>"
            },
            {
              "label": "Form",
              "mask": false,
              "tableView": true,
              "alwaysEnabled": false,
              "type": "form",
              "input": true,
              "key": "form2",
              "conditional": {
                "show": "",
                "when": "",
                "json": ""
              },
              "reference": false,
              "form": "5d7aa15b18fecd734051ae7f",
              "formRevision": "",
              "reorder": false,
              "customConditional": "",
              "logic": [
                
              ]
            },
            {
              "label": "Form",
              "mask": false,
              "tableView": true,
              "alwaysEnabled": false,
              "type": "form",
              "input": true,
              "key": "form6",
              "conditional": {
                "show": "",
                "when": "",
                "json": ""
              },
              "form": "5d4d377e9410f50010f3006e",
              "formRevision": "",
              "reorder": false,
              "customConditional": "",
              "logic": [
                
              ]
            },
            {
              "label": "Form",
              "mask": false,
              "tableView": true,
              "alwaysEnabled": false,
              "type": "form",
              "input": true,
              "key": "form7",
              "conditional": {
                "show": "",
                "when": "",
                "json": ""
              },
              "reference": false,
              "form": "5d5a7c9f669977001b5b617f",
              "formRevision": "",
              "reorder": false,
              "customConditional": "",
              "logic": [
                
              ]
            }
          ],
          "key": "panel",
          "conditional": {
            "show": "",
            "when": "",
            "json": ""
          },
          "collapsed": false,
          "reorder": false,
          "customConditional": "",
          "nextPage": "",
          "logic": [
            
          ]
        },
        {
          "label": "Page 2",
          "title": "Pagina 2",
          "breadcrumbClickable": true,
          "buttonSettings": {
            "previous": true,
            "cancel": true,
            "next": true
          },
          "collapsible": false,
          "mask": false,
          "tableView": false,
          "alwaysEnabled": false,
          "type": "panel",
          "key": "page2",
          "input": false,
          "components": [
            {
              "label": "Content",
              "className": "",
              "refreshOnChange": false,
              "mask": false,
              "tableView": true,
              "alwaysEnabled": false,
              "type": "content",
              "input": false,
              "key": "content5",
              "validate": {
                "customMessage": "",
                "json": ""
              },
              "conditional": {
                "show": "",
                "when": "",
                "json": ""
              },
              "reorder": false,
              "encrypted": false,
              "customConditional": "",
              "logic": [
                
              ],
              "html": "<p><span style=\"color:rgb(0,0,0);\">obbligato al pagamento dell’I.MU.P. per i seguenti immobili siti nel Comune di Rovereto:</span></p>"
            },
            {
              "label": "Fabbricati",
              "hideLabel": true,
              "disableAddingRemovingRows": false,
              "addAnother": "Aggiungi",
              "addAnotherPosition": "bottom",
              "removePlacement": "col",
              "defaultOpen": false,
              "layoutFixed": false,
              "enableRowGroups": false,
              "reorder": false,
              "mask": false,
              "tableView": true,
              "alwaysEnabled": false,
              "type": "datagrid",
              "input": true,
              "key": "fabbricati",
              "defaultValue": [
                {
                  "form3": {
                    "data": {
                      "columnsCC": "",
                      "columnsPEdOPF": "",
                      "columnsSub": "",
                      "columnsPossesso": "",
                      "columnsTipodiutilizzo": ""
                    }
                  }
                }
              ],
              "validate": {
                "customMessage": "",
                "json": ""
              },
              "conditional": {
                "show": "",
                "when": "",
                "json": ""
              },
              "components": [
                {
                  "label": "Fabbricati",
                  "mask": false,
                  "tableView": true,
                  "alwaysEnabled": false,
                  "type": "form",
                  "input": true,
                  "key": "form3",
                  "conditional": {
                    "show": "",
                    "when": "",
                    "json": ""
                  },
                  "form": "5d5a7e68669977001b5b6181",
                  "formRevision": "",
                  "customConditional": "",
                  "logic": [
                    
                  ],
                  "row": "0-0",
                  "reference": false,
                  "reorder": false
                }
              ],
              "encrypted": false,
              "customConditional": "",
              "logic": [
                
              ],
              "groupToggle": false
            }
          ],
          "conditional": {
            "show": "",
            "when": "",
            "json": ""
          },
          "collapsed": false,
          "reorder": false,
          "customConditional": "",
          "nextPage": "",
          "logic": [
            
          ]
        },
        {
          "label": "Page 3",
          "title": "Pagina 3",
          "breadcrumbClickable": true,
          "buttonSettings": {
            "previous": true,
            "cancel": true,
            "next": true
          },
          "collapsible": false,
          "mask": false,
          "tableView": false,
          "alwaysEnabled": false,
          "type": "panel",
          "key": "page3",
          "input": false,
          "components": [
            {
              "label": "Content",
              "className": "",
              "refreshOnChange": false,
              "mask": false,
              "tableView": true,
              "alwaysEnabled": false,
              "type": "content",
              "input": false,
              "key": "content6",
              "validate": {
                "customMessage": "",
                "json": ""
              },
              "conditional": {
                "show": "",
                "when": "",
                "json": ""
              },
              "reorder": false,
              "encrypted": false,
              "customConditional": "",
              "logic": [
                
              ],
              "html": "<p><span style=\"color:rgb(0,0,0);\">Dato atto che è stata versata la somma complessiva di</span></p>"
            },
            {
              "label": "Totale",
              "prefix": "€",
              "mask": false,
              "tableView": true,
              "alwaysEnabled": false,
              "type": "currency",
              "input": true,
              "key": "totale",
              "validate": {
                "customMessage": "",
                "json": ""
              },
              "conditional": {
                "show": "",
                "when": "",
                "json": ""
              },
              "delimiter": true,
              "currency": "EUR",
              "encrypted": false,
              "customConditional": "",
              "logic": [
                
              ],
              "reorder": false
            },
            {
              "label": "Comune",
              "prefix": "€",
              "mask": false,
              "tableView": true,
              "alwaysEnabled": false,
              "type": "currency",
              "input": true,
              "key": "comune",
              "validate": {
                "customMessage": "",
                "json": ""
              },
              "conditional": {
                "show": "",
                "when": "",
                "json": ""
              },
              "delimiter": true,
              "currency": "EUR",
              "encrypted": false,
              "customConditional": "",
              "logic": [
                
              ],
              "reorder": false
            },
            {
              "label": "Stato",
              "prefix": "€",
              "mask": false,
              "tableView": true,
              "alwaysEnabled": false,
              "type": "currency",
              "input": true,
              "key": "stato",
              "validate": {
                "customMessage": "",
                "json": ""
              },
              "conditional": {
                "show": "",
                "when": "",
                "json": ""
              },
              "delimiter": true,
              "currency": "EUR",
              "encrypted": false,
              "customConditional": "",
              "logic": [
                
              ],
              "reorder": false
            },
            {
              "label": "Content",
              "className": "",
              "refreshOnChange": false,
              "mask": false,
              "tableView": true,
              "alwaysEnabled": false,
              "type": "content",
              "input": false,
              "key": "content7",
              "validate": {
                "customMessage": "",
                "json": ""
              },
              "conditional": {
                "show": "",
                "when": "",
                "json": ""
              },
              "reorder": false,
              "encrypted": false,
              "customConditional": "",
              "logic": [
                
              ],
              "html": "<p><span style=\"color:rgb(0,0,0);\">ai sensi dell'art. 14 vigente Regolamento Comunale per la disciplina dell'I.MU.P., il trasferimento della somma di</span></p>"
            },
            {
              "label": "Form",
              "mask": false,
              "tableView": true,
              "alwaysEnabled": false,
              "type": "form",
              "input": true,
              "key": "form13",
              "conditional": {
                "show": "",
                "when": "",
                "json": ""
              },
              "reference": false,
              "form": "5d4d310a9410f50010f3006a",
              "formRevision": "",
              "reorder": false,
              "customConditional": "",
              "logic": [
                
              ]
            },
            {
              "label": "Content",
              "className": "",
              "refreshOnChange": false,
              "mask": false,
              "tableView": true,
              "alwaysEnabled": false,
              "type": "content",
              "input": false,
              "key": "content8",
              "validate": {
                "customMessage": "",
                "json": ""
              },
              "conditional": {
                "show": "",
                "when": "",
                "json": ""
              },
              "reorder": false,
              "encrypted": false,
              "customConditional": "",
              "logic": [
                
              ],
              "html": "<p><span style=\"color:rgb(0,0,0);\">a favore del Comune di&nbsp;</span></p>"
            },
            {
              "label": "Comune",
              "hideLabel": true,
              "allowMultipleMasks": false,
              "showWordCount": false,
              "showCharCount": false,
              "tableView": true,
              "alwaysEnabled": false,
              "type": "textfield",
              "input": true,
              "key": "comune2",
              "defaultValue": "",
              "validate": {
                "customMessage": "",
                "json": ""
              },
              "conditional": {
                "show": "",
                "when": "",
                "json": ""
              },
              "inputFormat": "plain",
              "encrypted": false,
              "customConditional": "",
              "logic": [
                
              ],
              "widget": {
                "type": ""
              },
              "reorder": false
            },
            {
              "label": "Content",
              "className": "",
              "refreshOnChange": false,
              "mask": false,
              "tableView": true,
              "alwaysEnabled": false,
              "type": "content",
              "input": false,
              "key": "content9",
              "validate": {
                "customMessage": "",
                "json": ""
              },
              "conditional": {
                "show": "",
                "when": "",
                "json": ""
              },
              "reorder": false,
              "encrypted": false,
              "customConditional": "",
              "logic": [
                
              ],
              "html": "<p><span style=\"color:rgb(0,0,0);\">per la seguente motivazione</span></p>"
            },
            {
              "label": "Motivazione",
              "hideLabel": true,
              "autoExpand": false,
              "isUploadEnabled": false,
              "showWordCount": false,
              "showCharCount": false,
              "tableView": true,
              "alwaysEnabled": false,
              "type": "textarea",
              "input": true,
              "key": "motivazione",
              "defaultValue": "",
              "validate": {
                "customMessage": "",
                "json": ""
              },
              "conditional": {
                "show": "",
                "when": "",
                "json": ""
              },
              "inputFormat": "plain",
              "encrypted": false,
              "customConditional": "",
              "logic": [
                
              ],
              "uploadUrl": "",
              "uploadOptions": "",
              "uploadDir": "",
              "reorder": false
            }
          ],
          "conditional": {
            "show": "",
            "when": "",
            "json": ""
          },
          "collapsed": false,
          "reorder": false,
          "customConditional": "",
          "nextPage": "",
          "logic": [
            
          ]
        }
      ],
      "tags": [
        "basic"
      ],
      "_id": "5d7a34027672e80020c95c01",
      "title": "Imup - modulo rimborso con trasferimento",
      "name": "imupModuloRimborsoConTrasferimento",
      "path": "imuprimborso",
      "description": "imupModuloRimborsoConTrasferimento",
      "modified": "2019-09-13T07:35:39.921Z",
      "created": "2019-09-12T12:03:14.454Z",
      "__v": 1
    }
    
Request:

`GET /printable/5d7a34027672e80020c95c02`

Response:
    
    404 Not Found (application/json)
    
    {
      "title": "Not Found",
      "detail": "Form not found",
      "status": 404,
      "instance": "/form/5d7a34027672e80020c95c02"
    }
    
Request:

`GET /form/123`

Response:
    
    400 Bad Request (application/json)
    
    {
      "title": "Bad Request",
      "detail": "Invalid Id",
      "status": 400,
      "instance": {
        "message": "Cast to ObjectId failed for value \"123\" at path \"_id\" for model \"Form\"",
        "name": "CastError",
        "stringValue": "\"123\"",
        "kind": "ObjectId",
        "value": "123",
        "path": "_id"
      }
    }
    
## Retrieve Form [`GET /{path}`]

Request:

`GET /nec`

Response:

    200 OK (application/json)
    
    {
        "display": "form",
        "type": "form",
        "components": [
            {
                "label": "Columns",
                "input": false,
                "tableView": false,
                "key": "columns",
                "columns": [
                    {
                        "components": [
                            {
                                "label": "Nome",
                                "placeholder": "Mario",
                                "allowMultipleMasks": false,
                                "showWordCount": false,
                                "showCharCount": false,
                                "clearOnHide": false,
                                "tableView": true,
                                "alwaysEnabled": false,
                                "type": "textfield",
                                "input": true,
                                "key": "nome",
                                "defaultValue": "",
                                "spellcheck": false,
                                "validate": {
                                    "pattern": "^([a-zA-Z ])+$",
                                    "customMessage": "Il nome può contenere solo caratteri alfabetici",
                                    "json": ""
                                },
                                "conditional": {
                                    "show": "",
                                    "when": "",
                                    "json": ""
                                },
                                "inputFormat": "plain",
                                "tags": [],
                                "encrypted": false,
                                "customConditional": "",
                                "logic": [],
                                "widget": {
                                    "type": ""
                                },
                                "reorder": false
                            }
                        ],
                        "width": 6,
                        "offset": 0,
                        "push": 0,
                        "pull": 0,
                        "type": "column",
                        "input": false,
                        "hideOnChildrenHidden": false,
                        "key": "column",
                        "tableView": true,
                        "label": "Column"
                    },
                    {
                        "components": [
                            {
                                "label": "Cognome",
                                "placeholder": "Rossi",
                                "allowMultipleMasks": false,
                                "showWordCount": false,
                                "showCharCount": false,
                                "clearOnHide": false,
                                "tableView": true,
                                "alwaysEnabled": false,
                                "type": "textfield",
                                "input": true,
                                "key": "cognome",
                                "defaultValue": "",
                                "spellcheck": false,
                                "validate": {
                                    "pattern": "^([a-zA-Z ])+$",
                                    "customMessage": "Il cognome può contenere solo caratteri alfabetici",
                                    "json": ""
                                },
                                "conditional": {
                                    "show": "",
                                    "when": "",
                                    "json": ""
                                },
                                "inputFormat": "plain",
                                "tags": [],
                                "widget": {
                                    "type": ""
                                },
                                "reorder": false,
                                "encrypted": false,
                                "customConditional": "",
                                "logic": []
                            }
                        ],
                        "width": 6,
                        "offset": 0,
                        "push": 0,
                        "pull": 0,
                        "type": "column",
                        "input": false,
                        "hideOnChildrenHidden": false,
                        "key": "column",
                        "tableView": true,
                        "label": "Column"
                    }
                ],
                "type": "columns",
                "hideLabel": true,
                "tags": [],
                "conditional": {
                    "show": ""
                }
            },
            {
                "label": "Submit",
                "state": "",
                "theme": "primary",
                "shortcut": "",
                "mask": false,
                "tableView": true,
                "alwaysEnabled": false,
                "type": "button",
                "input": true,
                "key": "submit2",
                "defaultValue": false,
                "validate": {
                    "customMessage": "",
                    "json": ""
                },
                "conditional": {
                    "show": "",
                    "when": "",
                    "json": ""
                },
                "showValidations": false,
                "event": "",
                "url": "",
                "custom": "",
                "reorder": false,
                "encrypted": false,
                "customConditional": "",
                "logic": []
            }
        ],
        "tags": [
            "component"
        ],
        "_id": "5d4d26ff9410f50010f30068",
        "title": "NeC",
        "name": "neC",
        "path": "nec",
        "description": "Nome e Cognome",
        "machineName": "neC",
        "modified": "2019-09-06T09:14:17.918Z",
        "created": "2019-09-06T09:14:17.918Z",
        "__v": 0
    }
    
Request:

`GET /asd`

Response:

    404 Not Found (application/json)
    
    {
        "title": "Not Found",
        "detail": "Form not Found",
        "status": 404,
        "instance": "/asd"
    }
    
## Create a Form [`POST /form`]

Request:

`POST /form`

    {
        "display": "form",
        "type": "form",
        "components": [],
        "tags": [
            "component"
        ],
        "title": "Prova",
        "name": "prova",
        "path": "prova",
        "description": "Form di Prova"
    }

Response:

    200 OK (application/json)
    
    {
        "display": "form",
        "type": "form",
        "components": [],
        "tags": [
            "component"
        ],
        "_id": "5d7a04353fbb8062f56c9baf",
        "title": "Prova",
        "name": "prova",
        "path": "prova",
        "description": "Form di Prova",
        "modified": "2019-09-12T08:39:17.923Z",
        "created": "2019-09-12T08:39:17.923Z",
        "__v": 0
    }
    
Request:

`POST /form`

    {
        "display": "form",
        "type": "form",
        "components": [],
        "tags": [
            "component"
        ],
        "name": "prova",
        "path": "prova",
        "description": "Form di Prova"
    }

Response:

    400 Bad Request (application/json)
    
    {
        "title": "Bad Request",
        "detail": {
            "errors": {
                "title": {
                    "message": "Path `title` is required.",
                    "name": "ValidatorError",
                    "properties": {
                        "message": "Path `title` is required.",
                        "type": "required",
                        "path": "title"
                    },
                    "kind": "required",
                    "path": "title"
                }
            },
            "_message": "Form validation failed",
            "message": "Form validation failed: title: Path `title` is required.",
            "name": "ValidationError"
        },
        "status": 400,
        "instance": {
            "display": "form",
            "type": "form",
            "components": [],
            "tags": [
                "component"
            ],
            "name": "prova",
            "path": "prova",
            "description": "Form di Prova"
        }
    }
    
## Delete a Form [`DELETE /{path}`]

Request:

`DELETE /prova`

Response:
    
    200 OK (application/json)
    
    {
        "display": "form",
        "type": "form",
        "components": [],
        "tags": [
            "component"
        ],
        "_id": "5d7a04353fbb8062f56c9baf",
        "title": "Prova",
        "name": "prova",
        "path": "prova",
        "description": "Form di Prova",
        "modified": "2019-09-12T08:39:17.923Z",
        "created": "2019-09-12T08:39:17.923Z",
        "__v": 0
    }

Request:

`DELETE /asd`

Response:

     404 Not Found (application/json)  
        
    {
        "title": "Not Found",
        "detail": "Form does not exists",
        "status": 404,
        "instance": "/asd"
    }
    
## Delete a Form By Form Id[`DELETE /form/{formId}`]

Request:

`DELETE /form/5d7a04353fbb8062f56c9baf`

Response:
    
    200 OK (application/json)
    
    {
        "display": "form",
        "type": "form",
        "components": [],
        "tags": [
            "component"
        ],
        "_id": "5d7a04353fbb8062f56c9baf",
        "title": "Prova",
        "name": "prova",
        "path": "prova",
        "description": "Form di Prova",
        "modified": "2019-09-12T08:39:17.923Z",
        "created": "2019-09-12T08:39:17.923Z",
        "__v": 0
    }

Request:

`DELETE /form/5d7a04353fbb8062f56c9ba3`

Response:

     404 Not Found (application/json)  
        
    {
        "title": "Not Found",
        "detail": "Form does not exists",
        "status": 404,
        "instance": "/form/5d7a04353fbb8062f56c9ba3"
    }

## Edit a Form [`PUT /{path}`]

Request:

`PUT /nec`

    {
        "display": "form",
        "type": "form",
        "components": [
            {
                "label": "Columns",
                "input": false,
                "tableView": false,
                "key": "columns",
                "columns": [
                    {
                        "components": [
                            {
                                "label": "Nome",
                                "placeholder": "Mario",
                                "allowMultipleMasks": false,
                                "showWordCount": false,
                                "showCharCount": false,
                                "clearOnHide": false,
                                "tableView": true,
                                "alwaysEnabled": false,
                                "type": "textfield",
                                "input": true,
                                "key": "nome",
                                "defaultValue": "",
                                "spellcheck": false,
                                "validate": {
                                    "pattern": "^([a-zA-Z ])+$",
                                    "customMessage": "Il nome può contenere solo caratteri alfabetici",
                                    "json": ""
                                },
                                "conditional": {
                                    "show": "",
                                    "when": "",
                                    "json": ""
                                },
                                "inputFormat": "plain",
                                "tags": [],
                                "encrypted": false,
                                "customConditional": "",
                                "logic": [],
                                "widget": {
                                    "type": ""
                                },
                                "reorder": false
                            }
                        ],
                        "width": 6,
                        "offset": 0,
                        "push": 0,
                        "pull": 0,
                        "type": "column",
                        "input": false,
                        "hideOnChildrenHidden": false,
                        "key": "column",
                        "tableView": true,
                        "label": "Column"
                    },
                    {
                        "components": [
                            {
                                "label": "Cognome",
                                "placeholder": "Rossi",
                                "allowMultipleMasks": false,
                                "showWordCount": false,
                                "showCharCount": false,
                                "clearOnHide": false,
                                "tableView": true,
                                "alwaysEnabled": false,
                                "type": "textfield",
                                "input": true,
                                "key": "cognome",
                                "defaultValue": "",
                                "spellcheck": false,
                                "validate": {
                                    "pattern": "^([a-zA-Z ])+$",
                                    "customMessage": "Il cognome può contenere solo caratteri alfabetici",
                                    "json": ""
                                },
                                "conditional": {
                                    "show": "",
                                    "when": "",
                                    "json": ""
                                },
                                "inputFormat": "plain",
                                "tags": [],
                                "widget": {
                                    "type": ""
                                },
                                "reorder": false,
                                "encrypted": false,
                                "customConditional": "",
                                "logic": []
                            }
                        ],
                        "width": 6,
                        "offset": 0,
                        "push": 0,
                        "pull": 0,
                        "type": "column",
                        "input": false,
                        "hideOnChildrenHidden": false,
                        "key": "column",
                        "tableView": true,
                        "label": "Column"
                    }
                ],
                "type": "columns",
                "hideLabel": true,
                "tags": [],
                "conditional": {
                    "show": ""
                }
            },
            {
                "label": "Submit",
                "state": "",
                "theme": "primary",
                "shortcut": "",
                "mask": false,
                "tableView": true,
                "alwaysEnabled": false,
                "type": "button",
                "input": true,
                "key": "submit2",
                "defaultValue": false,
                "validate": {
                    "customMessage": "",
                    "json": ""
                },
                "conditional": {
                    "show": "",
                    "when": "",
                    "json": ""
                },
                "showValidations": false,
                "event": "",
                "url": "",
                "custom": "",
                "reorder": false,
                "encrypted": false,
                "customConditional": "",
                "logic": []
            }
        ],
        "tags": [
            "component"
        ],
        "_id": "5d4d26ff9410f50010f30068",
        "title": "Nome e Cognome",
        "name": "neC",
        "path": "nec",
        "description": "Nome e Cognome",
        "machineName": "neC",
        "modified": "2019-09-11T06:44:54.940Z",
        "created": "2019-09-06T09:14:17.918Z",
        "__v": 6
    }
 
Response:

    200 OK (application/json)
    
    {
        "display": "form",
        "type": "form",
        "components": [
            {
                "label": "Columns",
                "input": false,
                "tableView": false,
                "key": "columns",
                "columns": [
                    {
                        "components": [
                            {
                                "label": "Nome",
                                "placeholder": "Mario",
                                "allowMultipleMasks": false,
                                "showWordCount": false,
                                "showCharCount": false,
                                "clearOnHide": false,
                                "tableView": true,
                                "alwaysEnabled": false,
                                "type": "textfield",
                                "input": true,
                                "key": "nome",
                                "defaultValue": "",
                                "spellcheck": false,
                                "validate": {
                                    "pattern": "^([a-zA-Z ])+$",
                                    "customMessage": "Il nome può contenere solo caratteri alfabetici",
                                    "json": ""
                                },
                                "conditional": {
                                    "show": "",
                                    "when": "",
                                    "json": ""
                                },
                                "inputFormat": "plain",
                                "tags": [],
                                "encrypted": false,
                                "customConditional": "",
                                "logic": [],
                                "widget": {
                                    "type": ""
                                },
                                "reorder": false
                            }
                        ],
                        "width": 6,
                        "offset": 0,
                        "push": 0,
                        "pull": 0,
                        "type": "column",
                        "input": false,
                        "hideOnChildrenHidden": false,
                        "key": "column",
                        "tableView": true,
                        "label": "Column"
                    },
                    {
                        "components": [
                            {
                                "label": "Cognome",
                                "placeholder": "Rossi",
                                "allowMultipleMasks": false,
                                "showWordCount": false,
                                "showCharCount": false,
                                "clearOnHide": false,
                                "tableView": true,
                                "alwaysEnabled": false,
                                "type": "textfield",
                                "input": true,
                                "key": "cognome",
                                "defaultValue": "",
                                "spellcheck": false,
                                "validate": {
                                    "pattern": "^([a-zA-Z ])+$",
                                    "customMessage": "Il cognome può contenere solo caratteri alfabetici",
                                    "json": ""
                                },
                                "conditional": {
                                    "show": "",
                                    "when": "",
                                    "json": ""
                                },
                                "inputFormat": "plain",
                                "tags": [],
                                "widget": {
                                    "type": ""
                                },
                                "reorder": false,
                                "encrypted": false,
                                "customConditional": "",
                                "logic": []
                            }
                        ],
                        "width": 6,
                        "offset": 0,
                        "push": 0,
                        "pull": 0,
                        "type": "column",
                        "input": false,
                        "hideOnChildrenHidden": false,
                        "key": "column",
                        "tableView": true,
                        "label": "Column"
                    }
                ],
                "type": "columns",
                "hideLabel": true,
                "tags": [],
                "conditional": {
                    "show": ""
                }
            },
            {
                "label": "Submit",
                "state": "",
                "theme": "primary",
                "shortcut": "",
                "mask": false,
                "tableView": true,
                "alwaysEnabled": false,
                "type": "button",
                "input": true,
                "key": "submit2",
                "defaultValue": false,
                "validate": {
                    "customMessage": "",
                    "json": ""
                },
                "conditional": {
                    "show": "",
                    "when": "",
                    "json": ""
                },
                "showValidations": false,
                "event": "",
                "url": "",
                "custom": "",
                "reorder": false,
                "encrypted": false,
                "customConditional": "",
                "logic": []
            }
        ],
        "tags": [
            "component"
        ],
        "_id": "5d4d26ff9410f50010f30068",
        "title": "Nome e Cognome",
        "name": "neC",
        "path": "nec",
        "description": "Nome e Cognome",
        "machineName": "neC",
        "modified": "2019-09-12T08:35:49.624Z",
        "created": "2019-09-06T09:14:17.918Z",
        "__v": 7
    }

Request:

`PUT /nec`
    
    {
    	"_id": "123"
    }
 
Response:

    400 Bad Request (application/json)
    
    {
        "title": "Bad Request",
        "detail": "Invalid parameter",
        "status": 400,
        "instance": {
            "message": "Cast to ObjectId failed for value \"123\" at path \"_id\" for model \"Form\"",
            "name": "CastError",
            "stringValue": "\"123\"",
            "kind": "ObjectId",
            "value": "123",
            "path": "_id"
        }
    }

   
## Retrieve All Submissions [`GET /{path}/submission`]

Request:

`GET /prova/submission`

Response:

    200 OK (application/json)
    
    [
        {
            "_id": "5d778cc5bce03d0020a16a1a",
            "data": {
                "textField2": "Prova",
                "textField3": "Prova"
            },
            "form": "5d77794fb76d04001f2813d7",
            "status": null,
            "modified": "2019-09-10T11:45:09.129Z",
            "created": "2019-09-10T11:45:09.129Z",
            "__v": 0
        },
        {
            "_id": "5d778cd5bce03d0020a16a1b",
            "data": {
                "textField2": "Test",
                "textField3": "Test"
            },
            "form": "5d77794fb76d04001f2813d7",
            "status": null,
            "modified": "2019-09-10T11:45:25.037Z",
            "created": "2019-09-10T11:45:25.037Z",
            "__v": 0
        }
    ]
    
Request:

`GET /test/submission`

Response

    404 Not Found (application/json)
    
    {
        "title": "Not Found",
        "detail": "Form not found",
        "status": 404,
        "instance": "/test/submission"
    }
    
## Retrieve Submission by id [`GET /{path}/submission/{submissionId}`]

Request:

`GET /prova/submission/5d778cc5bce03d0020a16a1a`

Response:

    200 OK (application/json)
    
    {
        "_id": "5d778cc5bce03d0020a16a1a",
        "data": {
            "textField2": "Prova",
            "textField3": "Prova"
        },
        "form": "5d77794fb76d04001f2813d7",
        "status": null,
        "modified": "2019-09-10T11:45:09.129Z",
        "created": "2019-09-10T11:45:09.129Z",
        "__v": 0
    }
    
Request:

`GET /prova/submission/123`

Response:

    400 Bad Request (application/json)
    
    {
        "title": "Bad Request",
        "detail": "Invalid id 123",
        "status": 400,
        "instance": {
            "message": "Cast to ObjectId failed for value \"123\" at path \"_id\" for model \"Submission\"",
            "name": "CastError",
            "stringValue": "\"123\"",
            "kind": "ObjectId",
            "value": "123",
            "path": "_id"
        }
    }
 
Request:

`GET /nec/submission/123`

Response:

    404 Not Found (application/json)
    
    {
        "title": "Not Found",
        "detail": "Submission with id 5d778cc5bce03d0020a16a1f not found",
        "status": 404,
        "instance": "/nec/submission/5d778cc5bce03d0020a16a1f"
    }
  
## Edit Submission [`PUT /{path}/{submission}/{submissionId}`]

Request:

`PUT /prova/submission/5d778cc5bce03d0020a16a1a`

    {
    	"data": {
    	        "textField2": "Prova2",
    	        "textField3": "Prova"
    	    }
    }

Response:

    200 OK (application/json)
    
    {
        "_id": "5d778cc5bce03d0020a16a1a",
        "data": {
            "data": {
                "textField2": "Prova2",
                "textField3": "Prova"
            }
        },
        "form": "5d77794fb76d04001f2813d7",
        "status": null,
        "modified": "2019-09-12T08:49:28.226Z",
        "created": "2019-09-10T11:45:09.129Z",
        "__v": 2
    }
    
Request:

`PUT /prova/submission/5d778cc5bce03d0020a16a1f`

Response:

    404 Not Found (application/json)
    
    {
        "title": "Not Found",
        "detail": "Submission with id 5d778cc5bce03d0020a16a1f not found",
        "status": 404,
        "instance": "/prova/submission/5d778cc5bce03d0020a16a1f"
    }
    
Request:

`PUT /prova/submission/123`

Response:

    400 Bad Request (application/json)
    
    {
        "title": "Bad Request",
        "detail": "Invalid id 123",
        "status": 400,
        "instance": {
            "message": "Cast to ObjectId failed for value \"123\" at path \"_id\" for model \"Submission\"",
            "name": "CastError",
            "stringValue": "\"123\"",
            "kind": "ObjectId",
            "value": "123",
            "path": "_id"
        }
    }
  
## Create Submission [`POST /{path}/submission`]

Request:

`POST /prova/submission`

    {
    	"data": {
                    "textField2": "Prova",
                    "textField3": "Prova"
                }
    }
    
Response: 
        
     200 OK (application/json)
     
     {
         "_id": "5d7a07f73fbb8062f56c9bb1",
         "data": {
             "data": {
                 "textField2": "Prova",
                 "textField3": "Prova"
             }
         },
         "form": "5d77794fb76d04001f2813d7",
         "status": null,
         "modified": "2019-09-12T08:55:19.369Z",
         "created": "2019-09-12T08:55:19.369Z",
         "__v": 0
     }

Request:

`POST /test/submission`

    {
    	"data": {
                    "textField2": "Prova",
                    "textField3": "Prova"
                }
    }
    
Response: 
        
     404 Not Found (application/json)
     
     {
         "title": "Not Found",
         "detail": "Form not found",
         "status": 404,
         "instance": "/test/submission"
     }
     
## Delete a Submission [`DELETE /{path}/submission/{submissionId}`]

Request:

`DELETE /nec/submission/5d7a08903fbb8062f56c9bb2`

Response:

    200 OK (application/json)
    
    {
        "_id": "5d7a08903fbb8062f56c9bb2",
        "form": "5d4d26ff9410f50010f30068",
        "status": null,
        "modified": "2019-09-12T08:57:52.299Z",
        "created": "2019-09-12T08:57:52.299Z",
        "__v": 0
    }
    
Request:

`DELETE /nec/submission/123`

Response:

    400 Bad Request (application/json)
    
    {
        "title": "Bad Request",
        "detail": "Invalid id 123",
        "status": 400,
        "instance": {
            "message": "Cast to ObjectId failed for value \"123\" at path \"_id\" for model \"Submission\"",
            "name": "CastError",
            "stringValue": "\"123\"",
            "kind": "ObjectId",
            "value": "123",
            "path": "_id"
        }
    }
   
Request:

`DELETE /asd/submission/5d7a08903fbb8062f56c9bb2`

Response:

    404 Not Found (application/json)
    
    {
        "title": "Not Found",
        "detail": "Submission Not Found",
        "status": 404,
        "instance": "/asd/submission/5d7a08903fbb8062f56c9bb2"
    }
     
    
## Retrieve Form Schema [`GET /{path}/schema`]

### Parameters
* path - Path of the form to retrieve

Request:

`GET /nec/schema`

Response:

    200 OK (application/json)
    
    {
      "name": {
        "label": "Nome",
        "type": "textfield"
      },
      "surname": {
        "label": "Cognome",
        "type": "textfield"
      }
    }
    
Request:

`GET /asd/schema`

Response:
    
    404 Not Found (application/json)
    
    {
      "title": "Not Found",
      "detail": "Form not found",
      "status": 404,
      "instance": "/asd/schema"
    }
     
## TAGS

Services are splitted into three main categories: 

* **Basic**: All Services offered by defauls
* **Component**: All Base Components that can be used to create more complex services
* **Custom**: All customized services.


#Environment

It is possible to configure the max-age and s-max-age parameters inside the cache-control header by defining 
the `MAX_AGE`, `S_MAX_AGE` parameters respectively
