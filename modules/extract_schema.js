const models = require('../models/form');
const waterfall = require('async-waterfall');

/**
 * Extracts submission schema from a given array of components
 * @param components: array of components to extract schema from
 * @param callback: function called when schema is ready
 */
function extractSchema(components, callback) {
  if (components.length === 0) {
    return callback(null)
  }
  let numComponents = components.length;
  let completed = 0;
  waterfall([
    function(next) {
      let schema = {};
      components.forEach(function(component) {
            if (component.form) {
              // Nested Form
              models.Form.findById(component.form).then(function(nestedForm) {
                // Extract Schema from Nested Form
                if (!nestedForm) {
                  completed += 1;
                  if (completed === numComponents) {
                    next(null, null);
                  }
                } else {
                  extractSchema(nestedForm.components, function(nestedSchema) {
                    schema[component.key] = {data: nestedSchema};
                    completed += 1;
                    if (completed === numComponents) {
                      next(null, schema);
                    }
                  });
                }
              });
            } else if (!component.input || component.input === 'false') {
              // Layout
              // COLUMNS
              if (component.type === 'columns') {
                extractSchema(component.columns, function(results) {
                  schema = {...schema, ...results};
                  completed += 1;
                  if (completed === numComponents) {
                    next(null, schema);
                  }
                });
                // COLUMN, FIELDSET, PANEL, WELL
              } else if (['column', 'fieldset', 'panel', 'well'].includes(
                  component.type)) {
                extractSchema(component.components, function(results) {
                  schema = {...schema, ...results};
                  completed += 1;
                  if (completed === numComponents) {
                    next(null, schema);
                  }
                });
                // TABLE
              } else if (component.type === 'table') {
                let _ = 0;
                if (component.numRows === undefined || component.numRows === null ) {
                  component.numRows = component.rows.length;
                }
                if (component.numCols === undefined || component.numCols === null && component.rows.length > 0) {
                  component.numCols = component.rows[0].length
                }
                component.rows.forEach(function(row) {
                  row.forEach(function(cell) {
                    extractSchema(cell.components, function(results) {
                      schema = {...schema, ...results};
                      _ += 1;
                      if (_ === (component.numRows * component.numCols)) {
                        completed += 1;
                        if (completed === numComponents) {
                          next(null, schema);
                        }
                      }
                    });
                  });
                });
                // TABS
              } else if (component.type === 'tabs') {
                let numTabs = 0;
                component.components.forEach(function(tab) {
                  extractSchema(tab.components, function(results) {
                    schema = {...schema, ...results};
                    numTabs += 1;
                    if (numTabs === tab.components.length) {
                      completed += 1;
                      if (completed === numComponents) {
                        next(null, schema);

                      }
                    }
                  });
                });
                // CONTENT, HTML
              } else if (['content', 'htmlelement'].includes(component.type)) {
                completed += 1;
                if (completed === numComponents) {
                  next(null, schema);
                }
              } else if (component.components) {
                //input not defined
                extractSchema(component.components, function(results) {
                  schema = {...schema, ...results};
                  completed += 1;
                  if (completed === numComponents) {
                    next(null, schema);

                  }

                });
              }
              // Input Type
            } else if (['datagrid', 'editgrid'].includes(component.type)) {
              extractSchema(component.components, function(results) {
                schema[component.key] = [results];
                completed += 1;
                if (completed === numComponents) {
                  next(null, schema);
                }
              });
            } else {
              // Standard component: no subcomponents
              if (component.type === 'selectboxes') {
                schema[component.key] = {};
                component.values.forEach(function(value) {
                  schema[component.key][value.value] = value.label;
                });
                completed += 1;
                if (completed === numComponents) {
                  next(null, schema);
                }
              } else if (component.type === 'button') {
                // skip buttons
                completed += 1;
                if (completed === numComponents) {
                  next(null, schema);
                }
              } else {
                // Add type inside schema
                schema[component.key] = {
                  label: component.label,
                  type: component.type
                };
                // Old version, no type
                // schema[component.key] = component.label;
                completed += 1;
                if (completed === numComponents) {
                  next(null, schema);
                }
              }
            }
          },
      );
    },
    function(schema) {
      callback(schema);
    },
  ]);
}

module.exports = extractSchema;
