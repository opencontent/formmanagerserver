const express = require('express');
const bodyParser = require('body-parser');
const response = require('../modules/json_response');
const extractSchema = require('../modules/extract_schema');
const models = require('../models/form');

const router = express.Router();

router.use(bodyParser.json());

// Get form by id
router.route('/form/:id')
  // getForm: retrieve a form
  .get(function (req, res) {
    models.Form.findById(req.params.id).then(function (form) {
      if (form) {
        response(res, 'OK', form, 200);
      } else {
        response(res, 'Form Not Found', req.originalUrl, 404);
      }
    }).catch(function (err) {
      response(res, 'Invalid Id', err, 400);
    });
  })
  // Deletes a form by Id
  .delete(function (req, res) {
    let id = req.params.id;

    models.Form.findOneAndRemove({_id: id}).then(function (form) {
      if (form) {
        response(res, 'OK', form, 200);
      } else {
        response(res, 'Form Not Found', req.originalUrl, 404);
      }
    }).catch(function (err) {
      response(res, `Invalid id ${id}`, err, 400);
    });
  });

router.route('/form/:id/schema')
  // getSchema: compute a form Schema
  .get(function (req, res) {
    models.Form.findById(req.params.id).then(function (form) {
      if (form) {
        extractSchema(form.components, function (schema) {
          response(res, 'OK', schema, 200);
        });
      } else {
        response(res, 'Form not found', req.originalUrl, 404);
      }
    }).catch(function (err) {
      response(res, 'Invalid Id', err, 400);
    });
  });

router.route('/printable/:id')
  // getSchema: compute a form Schema
  .get(function (req, res) {
    models.Form.findById(req.params.id).then(function (form) {
      if (form) {
        response(res, 'OK', wizardConverter(form), 200);
      } else {
        response(res, 'Form not found', req.originalUrl, 404);
      }
    }).catch(function (err) {
      response(res, 'Invalid Id', err, 400);
    });
  });

router.route(/^\/[a-zA-Z0-9_-]+\/schema$/)
  // Get schema
  .get(function (req, res) {
    const path = req.originalUrl.split('/', 2).pop();
    models.Form.findOne({path: path}).then(function (form) {
      if (form) {
        extractSchema(form.components, function (schema) {
          response(res, 'OK', schema, 200);
        });
      } else {
        response(res, 'Form not found', req.originalUrl, 404);
      }
    }).catch(function (err) {
      response(res, 'Invalid path', err, 400);
    });
  });

router.route('/form/')
  // listConsents: list forms registered
  .get(function (req, res) {
    // Default: exclude components
    var options = {
      'tags': {'$nin': ['custom']},
    };
    var selected = {};
    if (req.query.name) {
      options.name = {
        '$regex': new RegExp('.*' + req.query.name + '.*', 'i'),
      };
    }
    if (req.query.select) {
      req.query.select.split(',').forEach(function (select) {
        selected[select] = true;
      });
    }
    if (req.query.exclude_tags) {
      let excluded = [];
      req.query.exclude_tags.split(',').forEach(function (tag) {
        excluded.push(tag);
      });
      options.tags.$nin = excluded;
    }

    models.Form.find(options).select(selected).then(function (results) {
      response(res, 'OK', results, 200);
    }).catch(function (err) {
      response(res, 'Bad request', err, 400);
    });
  })
  // createConsent: create a form
  .post(function (req, res) {
    models.Form.create(req.body).then(function (form) {
      response(res, 'Created', form, 201);
    }).catch(function (err) {
      response(res, err, req.body, 400);
    });
  });

router.route('/:path')
  // Get form by path
  .get(function (req, res) {
    models.Form.findOne({path: req.params.path}).then(function (result) {
      if (result) {
        response(res, 'OK', result, 200);
      } else {
        response(res, 'Form not Found', req.originalUrl, 404);
      }
    }).catch(function (err) {
      response(res, 'Invalid path', err, 400);
    });
  })
  // Edit Form
  .put(function (req, res) {
    models.Form.findById(req.body._id).then(function (form) {
      if (form) {
        form.title = req.body.title;
        form.name = req.body.name;
        form.path = req.body.path;
        form.description = req.body.description;
        form.display = req.body.display;
        form.tags = req.body.tags;
        form.components = req.body.components;
        form.modified = Date.now();
        form.save();
        response(res, 'OK', form, 200);
      } else {
        response(res, 'The requested form does not exists', req.originalUrl,
          404);
      }
    }).catch(function (err) {
      response(res, 'Invalid parameter', err, 400);
    });
  })
  // delete Form
  .delete(function (req, res) {
    models.Form.findOneAndRemove({path: req.params.path}).then(function (form) {
      if (form) {
        response(res, 'OK', form, 200);
      } else {
        response(res, 'Invalid parameter path', req.originalUrl, 400);
      }
    });
  });

router.route(/^\/[a-zA-Z0-9_-]+\/submission$/)
  // Get submissions
  .get(function (req, res) {
    const path = req.originalUrl.split('/', 2).pop();
    models.Form.findOne({path: path}).select('_id').then(function (form) {
      if (form) {
        models.Submission.find({form: form._id}).then(function (results) {
          response(res, 'OK', results, 200);
        }).catch(function (err) {
          response(res, 'Invalid form', err, 400);
        });
      } else {
        response(res, 'Form not found', req.originalUrl, 404);
      }
    });
  }).post(function (req, res) {
  // Extract path
  const path = req.originalUrl.split('/', 2).pop();
  models.Form.findOne({path: path}).select('_id').then(function (form) {
    if (form) {
      delete req.body['submit'];
      models.Submission.create({
        data: req.body,
        form: form._id,
        status: null,
      }).then(function (submission) {
        response(res, 'OK', submission, 201);
      }).catch(function (err) {
        response(res, 'Invalid data', err, 400);
      });
    } else {
      response(res, 'Form not found', req.originalUrl, 404);
    }
  });
});

router.route(/^\/[a-zA-Z0-9_-]+\/submission\/.[^\/]+\/?$/)
  // get submission by id
  .get(function (req, res) {
    const id = req.originalUrl.split('/', 4).pop();
    models.Submission.findById(id).then(function (submission) {
      if (submission) {
        response(res, 'OK', submission, 200);
      } else {
        response(res, `Submission with id ${id} not found`, req.originalUrl,
          404);
      }
    }).catch(function (err) {
      response(res, `Invalid id ${id}`, err, 400);
    });
  })
  // Edit submission
  .put(function (req, res) {
    const id = req.originalUrl.split('/', 4).pop();
    models.Submission.findById(id).then(function (submission) {
      if (submission) {
        submission.data = req.body;
        submission.modified = Date.now();
        submission.save();
        response(res, 'OK', submission, 200);
      } else {
        response(res, `Submission with id ${id} not found`, req.originalUrl,
          404);
      }
    }).catch(function (err) {
      response(res, `Invalid id ${id}`, err, 400);
    });
  })
  // Delete Submission
  .delete(function (req, res) {
    const id = req.originalUrl.split('/', 4).pop();
    models.Submission.findOneAndRemove({_id: id}).then(function (form) {
      if (form) {
        response(res, 'OK', form, 200);
      } else {
        response(res, 'Submission Not Found', req.originalUrl, 404);
      }
    }).catch(function (err) {
      response(res, `Invalid id ${id}`, err, 400);
    });
  });

module.exports = router;

function wizardConverter(form) {
  if (form.display === 'wizard') {
    let converted = JSON.parse(JSON.stringify(form));
    // Metodo 1: conversione semplice: espansione delle pagine in verticale
    converted.display = 'form';

    return disable(converted);
  } else return disable(form);
}

function disable(form, callback) {
  let disabledForm = JSON.parse(JSON.stringify(form))

  if (disabledForm.components) {
    // Disable form components
    disabledForm.components.forEach(function (component) {
      if (component.components) {
        component.components.forEach(function (nested_component, index) {
          component.components[index] = disable(nested_component);
        })
      } else {
        component = disable(component);
      }
    })
  } else if (disabledForm.columns) {
    // Disable columns components
    disabledForm.columns.forEach(function (column) {
      if (column.components) {
        column.components.forEach(function (nested_component, index) {
          column.components[index] = disable(nested_component);
        })
      }
    })
  } else {
    // Simple component
    // Disable select populated via API
    if (disabledForm.type === 'select' && disabledForm['dataSrc'] === 'url') {
      disabledForm.type = "textfield"
    }
    // Disable JS custom default values
    if (disabledForm.customDefaultValue) {
      disabledForm.customDefaultValue = '';
    }
    // Disable JS custom calculated values
    if (disabledForm.calculateValue) {
      disabledForm.calculateValue = '';
    }
    // Disable JS validation
    if (disabledForm.validate) {
      disabledForm.validate.custom = '';
    }
  }
  return disabledForm;
}
